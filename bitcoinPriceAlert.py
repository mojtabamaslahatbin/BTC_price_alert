import requests


def sendAlert(price):
    url =  "SMS service provider's API_URL"
    payload = {'receptor': 'sms_receiver_phone_number','message': 'price is as low as your targetPrice!'}
    requests.post(url, data=payload)


# ? is price for your target (int or float)
targetPrice = ?

#in case of Error 400 , should set proxy
response = requests.get('bitcoin price check API_URL',proxies={'https': 'socks5://127.0.0.1:1080'})

#Extract bitcoin Price from received json data
price = float(response.json()['data']['amount'])

if (price <= targetPrice):
    sendAlert(price)
